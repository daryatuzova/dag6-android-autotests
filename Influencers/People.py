import time
from datetime import datetime
import SBTests
import Config
import sys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException, TimeoutException
import re

class Vakmensen(SBTests.TestCase):

    def test_vakmensen_follow(self):
        driver = self.driver
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.XPATH, '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]')))
        driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]').click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.FrameLayout[4]/android.widget.LinearLayout[1]/android.widget.TextView[1]')))
        driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.FrameLayout[4]/android.widget.LinearLayout[1]/android.widget.TextView[1]').click()
        for i in range(5):
            driver.swipe(400, 200, 200, 200)
            time.sleep(1)
            i += 1
        for i in range(5):
            driver.swipe(200, 200, 400, 200)
            time.sleep(1)
            i += 1
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.ID, 'button_follow')))
        follow = driver.find_element_by_id('button_follow').get_attribute('checked')
        print  follow
        if follow == "false":
            # Followersberfore
            Followers1 = driver.find_element_by_id('text_followers_count').get_attribute("text")
            FollowersCounter1 = re.findall('\d+', Followers1)
            for i in FollowersCounter1:
                print "Followers in overview card"+i
            WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.ID, 'button_follow')))
            driver.find_element_by_id('button_follow').click()
            followed = driver.find_element_by_id('button_follow').get_attribute('text')
            if followed <> "Volgend":
                print "Button should change the name to volgenD"
                assert True
            # Followers after
            Followers2 = driver.find_element_by_id('text_followers_count').get_attribute("text")
            FollowersCounter2 = re.findall('\d+', Followers2)
            for k in FollowersCounter2:
                for i in FollowersCounter1:
                    print "Followers after following incluencer: "+k
                    if int(k) - int(i) <> 1:
                        print "Followers counter wasn't increased"
                        assert False
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'text_bio')))
            driver.find_element_by_id('text_bio').click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'text_name')))
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'text_description')))
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'text_bio')))
            text = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'text'))).get_attribute("text")
            #WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'text_quote')))
            assert "Meer artikelen van" in text
            #Followers inside article
            Followers3 = driver.find_element_by_id('text_followers_count').get_attribute("text")
            FollowersCounter3 = re.findall('\d+', Followers3)
            for l in FollowersCounter3:
                for i in FollowersCounter1:
                    print "Followers inside follower's page: "+l
                    if int(l) - int(i) <> 1:
                        print "Followers counter wasn't increased inside"
                        assert False

    def test_vakmensen_unfollow(self):
        driver = self.driver
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.ID, 'button_follow')))
        # Followersberfore
        Followers1 = driver.find_element_by_id('text_followers_count').get_attribute("text")
        FollowersCounter1 = re.findall('\d+', Followers1)
        for i in FollowersCounter1:
            print "Followers in overview card(i) " + i
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.ID, 'button_follow')))
        driver.find_element_by_id('button_follow').click()
        Button = driver.find_element_by_id('button_follow').get_attribute('text')
        if Button<>"Volgen":
            print "Influenceer wasn't unfollwed"
            assert False
        Followers2 = driver.find_element_by_id('text_followers_count').get_attribute("text")
        FollowersCounter2 = re.findall('\d+', Followers2)
        for k in FollowersCounter2:
            for i in FollowersCounter1:
                print "Followers after UNfollowing incluencer(k): " + k
                if int(i) - int(k) <> 1:
                    print "Followers counter wasn't decrease"
                    assert False
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'button_close')))
        driver.find_element_by_id("button_close").click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'button_follow')))
        follow = driver.find_element_by_id('button_follow').get_attribute('checked')
        print  follow
        if follow <> "false":
            assert "Follow button wasn't updated"
            assert False
        Followers3 = driver.find_element_by_id('text_followers_count').get_attribute("text")
        FollowersCounter3 = re.findall('\d+', Followers3)
        for l in FollowersCounter3:
            for k in FollowersCounter2:
                print "Followers inside follower's page(l): " + l
                if int(k)<> int(l):
                    print "Followers counter wasn't updated"
                    assert False


