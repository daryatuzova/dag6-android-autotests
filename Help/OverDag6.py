__author__ = 'Darya Tuzova'
import time
from datetime import datetime
import SBTests
import Config
import sys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import re
from Variable import Variables
from selenium.common.exceptions import NoSuchElementException, TimeoutException

class OverDag(SBTests.TestCase):

    def test_help(self):
        driver = self.driver
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]')))
        driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]').click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.FrameLayout[6]/android.widget.LinearLayout[1]/android.widget.TextView[1]')))
        driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.FrameLayout[6]/android.widget.LinearLayout[1]').click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'section_faq')))
        driver.find_element_by_id('section_faq').click()
        i = 0
        for i in range(5):
            driver.swipe(200, 300, 200, 100)
            i += 1
        time.sleep(1)
        driver.press_keycode(4)
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'section_contact')))
        driver.find_element_by_id('section_contact').click()
        time.sleep(1)
        driver.press_keycode(4)
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'section_privacy')))
        driver.find_element_by_id('section_privacy').click()
        i = 0
        for i in range(5):
            driver.swipe(200, 300, 200, 100)
            i += 1
        time.sleep(1)
        driver.press_keycode(4)
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'section_policy')))
        driver.find_element_by_id('section_policy').click()
        i = 0
        for i in range(5):
            driver.swipe(200, 300, 200, 100)
            i += 1
        time.sleep(1)
        driver.press_keycode(4)
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'section_app_version')))
        driver.find_element_by_id('section_app_version').click()
        i = 0
        for i in range(5):
            driver.swipe(200, 300, 200, 100)
            i += 1
        time.sleep(1)
        driver.press_keycode(4)



