__author__ = 'Darya Tuzova'
import time
from datetime import datetime
import SBTests
import Config
import sys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver import ActionChains

class Zoeken(SBTests.TestCase):

    def test_search_special_characters(self):
        driver = self.driver
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.XPATH, '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]')))
        driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]').click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.FrameLayout[5]/android.widget.LinearLayout[1]/android.widget.TextView[1]')))
        driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.FrameLayout[5]/android.widget.LinearLayout[1]/android.widget.TextView[1]').click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'edit_text_search')))
        driver.find_element_by_id("edit_text_search").send_keys(',./?":%;\"')
        driver.hide_keyboard()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@text="Artikelen"]')))
        driver.find_element_by_xpath('//*[@text="Artikelen"]').click()
        try:
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'image_empty_icon')))
        except TimeoutException:
            print "Special characters shouldn't return any result"
            assert False
            pass
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@text="Vakmensen"]')))
        driver.find_element_by_xpath('//*[@text="Vakmensen"]').click()
        try:
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'image_empty_icon')))
        except TimeoutException:
            print "Special characters shouldn't return any result"
            assert False
            pass
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@text="Tags"]')))
        driver.find_element_by_xpath('//*[@text="Tags"]').click()
        try:
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'image_empty_icon')))
        except TimeoutException:
            print "Special characters shouldn't return any result"
            assert False
            pass

    def test_search_tags(self):
        driver = self.driver
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.XPATH, '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]')))
        driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]').click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH,'//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.FrameLayout[5]/android.widget.LinearLayout[1]/android.widget.TextView[1]')))
        driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.FrameLayout[5]/android.widget.LinearLayout[1]/android.widget.TextView[1]').click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'edit_text_search')))
        driver.find_element_by_id("edit_text_search").send_keys('Trending')
        driver.hide_keyboard()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@text="Tags"]')))
        driver.find_element_by_xpath('//*[@text="Tags"]').click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'text_tag_name')))
        driver.find_element_by_id("text_tag_name").click()
        try:
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'image_story_background')))
        except TimeoutException:
            print "Something is wrong, treding articles are not found"
            assert False
            pass

    def test_search_article(self):
        driver = self.driver
        time.sleep(2)
        driver.press_keycode(4)
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]')))
        driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]').click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH,'//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.FrameLayout[5]/android.widget.LinearLayout[1]/android.widget.TextView[1]')))
        driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.FrameLayout[5]/android.widget.LinearLayout[1]/android.widget.TextView[1]').click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'edit_text_search')))
        driver.find_element_by_id("edit_text_search").send_keys('op internet')
        driver.hide_keyboard()
        WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH, '//*[@text="Artikelen"]')))
        driver.find_element_by_xpath('//*[@text="Artikelen"]').click()
        try:
            WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.ID, 'button_bookmark')))
        except TimeoutException:
            print "Something is wrong, treding articles are not found"
            assert False
            pass

    def test_search_people(self):
        driver = self.driver
        # WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.ID, 'toolbar')))
        # Menu = driver.find_element_by_id('toolbar')
        # ActionChains(driver).move_to_element_with_offset(Menu, 20, 20).click().perform()
        time.sleep(2)
        driver.press_keycode(4)
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH,
                                                                        '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]')))
        driver.find_element_by_xpath(
            '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]').click()

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH,'//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.FrameLayout[5]/android.widget.LinearLayout[1]/android.widget.TextView[1]')))
        driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.FrameLayout[5]/android.widget.LinearLayout[1]/android.widget.TextView[1]').click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'edit_text_search')))
        driver.find_element_by_id("edit_text_search").send_keys('Kee')
        driver.hide_keyboard()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@text="Vakmensen"]')))
        driver.find_element_by_xpath('//*[@text="Vakmensen"]').click()
        try:
            WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.ID, 'button_follow')))
        except TimeoutException:
            print "Something is wrong, influencers are not found"
            assert False
            pass



