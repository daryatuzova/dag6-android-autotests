__author__ = 'Darya Tuzova'
import time
from datetime import datetime
import SBTests
import Config
import sys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException,TimeoutException


class Registration(SBTests.TestCase):

      def test_regeister_email(self):
        T = time.strftime('%H%M%S')
        T = datetime.now().microsecond
        T = str(T)
        print T
        driver = self.driver
        #register via email
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'image_icon')))
        driver.find_element_by_id("image_icon").click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ScrollView[1]/android.widget.RelativeLayout[1]/android.view.ViewGroup[1]/android.widget.CheckedTextView[1]')))
        driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ScrollView[1]/android.widget.RelativeLayout[1]/android.view.ViewGroup[1]/android.widget.CheckedTextView[1]').click()
        driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ScrollView[1]/android.widget.RelativeLayout[1]/android.view.ViewGroup[1]/android.widget.CheckedTextView[2]').click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'image_icon')))
        driver.find_element_by_id("image_icon").click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'image_icon')))
        driver.save_screenshot(Config.screen)
        driver.find_element_by_id("image_icon").click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'login_email')))
        driver.save_screenshot(Config.screen)
        driver.find_element_by_id('login_email').click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'first_name')))
        driver.save_screenshot(Config.screen)
        driver.find_element_by_id("first_name").send_keys("Beyonce Giselle")
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'last_name')))
        driver.find_element_by_id("last_name").send_keys("Knowles-Carter")
        driver.hide_keyboard()
        try:
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'image_icon')))
            driver.find_element_by_id("image_icon").click()
        except TimeoutException:
            time.sleep(1)
            driver.press_keycode(4)
            driver.find_element_by_id("image_icon").click()
        driver.save_screenshot(Config.screen)
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'email')))
        driver.find_element_by_id("email").send_keys("darya.tuzova"+T+"@salesboard.biz")
        driver.hide_keyboard()
        try:
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'image_icon')))
            driver.find_element_by_id("image_icon").click()
        except TimeoutException:
            time.sleep(1)
            driver.press_keycode(4)
            driver.find_element_by_id("image_icon").click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'password')))
        driver.find_element_by_id("password").send_keys("12345678")
        driver.hide_keyboard()
        try:
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'image_icon')))
            driver.find_element_by_id("image_icon").click()
        except TimeoutException:
            time.sleep(1)
            driver.press_keycode(4)
            driver.find_element_by_id("image_icon").click()
        driver.save_screenshot(Config.screen)
        try:
            WebDriverWait(driver, 2).until(EC.presence_of_element_located((By.ID, 'intercom_toolbar_close')))
            driver.find_element_by_id('intercom_toolbar_close').click()
        except TimeoutException:
            pass
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'image_icon')))
        driver.find_element_by_id("image_icon").click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'button_follow')))
        driver.find_element_by_id("button_follow").click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'next')))
        driver.find_element_by_id('next').click()
        #driver.find_element_by_id('intercom_toolbar_close').click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'button_confirm')))
        driver.save_screenshot(Config.screen)
        driver.find_element_by_id('button_confirm').click()
        driver.save_screenshot(Config.screen)

      def test_skip_registration(self):
          time.sleep(2)
          driver = self.driver
          driver.find_element_by_id("image_icon").click()
          time.sleep(5)
          driver.find_element_by_xpath('//*[@text="tech"]').click()
          time.sleep(1)
          driver.find_element_by_id("image_icon").click()
          time.sleep(1)
          driver.find_element_by_id("skip").click()
          time.sleep(3)
          cancel = driver.find_element_by_id("button2") .get_attribute("text")
          goon = driver.find_element_by_id("button1").get_attribute("text")
          message = driver.find_element_by_id("message").get_attribute("text")
          print cancel
          print goon
          print message
          assert cancel == "Registreer"
          assert goon == "Ga door"
          assert message == "Weet je het zeker? Je mist een hoop mooie artikelen."
          driver.find_element_by_id("button1").click()
          time.sleep(1)
          driver.find_element_by_id("next").click()







