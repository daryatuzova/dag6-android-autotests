__author__ = 'Darya Tuzova'

import unittest
import sys
import Config
import time
import os

from appium import webdriver
import TestResults

desired_capabilities = None
DRIVER = None


def getOrCreateWebdriver(self):
    global DRIVER
    global desired_capabilities
    desired_caps = {}
    desired_caps['platformName'] = "Android"
    desired_caps['platformVersion'] = sys.argv[6]
    desired_caps['deviceName'] = sys.argv[4]
    desired_caps['app'] = Config.apkpath
    desired_caps['udid']=sys.argv[8]
    desired_caps['appPackage']= 'nl.endeo.dag6'
    desired_caps['appActivity']= 'nl.endeo.dag6.splash.SplashActivity'
    desired_caps['autoAcceptAlerts'] = True

    print "Executing Local tests with APPIUM..."
    DRIVER = DRIVER or webdriver.Remote('http://localhost:'+sys.argv[2]+'/wd/hub', desired_caps)

    return DRIVER


class TestCase(unittest.TestCase):

    def setUp(self):
        self.driver = getOrCreateWebdriver(self)
        dateTimeStamp = time.strftime('%Y-%m-%d %H_%M')
        screen =  os.path.join(Config.screenshotpath,sys.argv[8]+"_"+dateTimeStamp+".jpg")
        self.driver.save_screenshot(screen)

    def tearDown(self):
        if sys.exc_info() != (None, None, None):
            TestResults.errors += 1;
            TestResults.total_errors += 1;

    def finish(self):
        print >>sys.stderr, '\n%s' % TestResults.errors, " errors at this test suite"
        TestResults.errors = 0;
        self.driver = None

        global DRIVER
        DRIVER = None