import SBTests
import Config
import sys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import  TimeoutException

class ReadingList(SBTests.TestCase):

    def test_reading_list(self):
        driver = self.driver
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]')))
        driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]').click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.FrameLayout[3]/android.widget.LinearLayout[1]/android.widget.TextView[1]')))
        driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.FrameLayout[3]/android.widget.LinearLayout[1]/android.widget.TextView[1]').click()
        try:
            WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.ID, 'button_bookmark')))
        except TimeoutException:
            print "Something is wrong, article was not added to Reading list"
            assert False
            pass
        driver.find_element_by_id('button_bookmark').click()
        NothingFound = driver.find_element_by_id('text_empty_title').get_attribute("text")
        if NothingFound <>"Dit is je leeslijst, hier kan je artikelen opslaan.":
             print "Article should disappear after it has been unbookmarked"
             assert False
        driver.find_element_by_id("snackbar_action").click()
        try:
            WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.ID, 'button_bookmark')))
        except TimeoutException:
            print "UNDO doesn't work"
            assert False
            pass

