__author__ = 'Darya Tuzova'
import time
import SBTests
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import Config
from Variable import Variables
from selenium.common.exceptions import NoSuchElementException,TimeoutException


class LoginTest(SBTests.TestCase):

      def test_onboarding_page(self):
        driver = self.driver
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'button_terms')))
        driver.find_element_by_id("button_terms").click()
        i = 0
        for i in range(5):
          driver.swipe(200, 200, 200, 10)
          i += 1
        driver.press_keycode(4)
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'button_privacy')))
        driver.find_element_by_id("button_privacy").click()
        time.sleep(2)
        i=0
        for i in range(5):
          driver.swipe(200,200,200,10)
          i+=1
        driver.press_keycode(4)
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'text_eo_moto')))
        element = driver.find_element_by_id("text_eo_moto")
        logo_text=element.get_attribute("text")
        print "Checking the text on the first page: test will fail if \"elke dag is nieus\" is not displayed"
        print "Actual result:"
        print logo_text
        assert 'elke dag' in logo_text
        assert 'is nieuws' in logo_text
        text_title = driver.find_element_by_id("text_title").get_attribute("text")
        print "Checking the text on the first page: test will fail if \"Begin je dag\" is not displayed"
        print "Actual result:"
        print text_title
        assert text_title=='Begin je dag'
        have_account= driver.find_element_by_id("text_have_account").get_attribute("text")
        print "Checking the text on the first page: test will fail if \"Heb je al een account?\" is not displayed"
        print "Actual result:"
        print have_account
        assert have_account == 'Heb je al een account?'
        text_terms_titled = driver.find_element_by_id("text_terms_title").get_attribute("text")
        print "Checking the text on the first page: test will fail if \"Door verder te gaan accepteer je de?\" is not displayed"
        print "Actual result:"
        print text_terms_titled
        assert  text_terms_titled == 'Door verder te gaan accepteer je de'


      def test_login_email(self):
        driver = self.driver
        #login via email
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'login')))
        driver.find_element_by_id("login").click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'login_email')))
        driver.find_element_by_id('login_email').click()
        try:
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'image_icon')))
          driver.find_element_by_id("image_icon").click()
        except TimeoutException:
          time.sleep(1)
          driver.press_keycode(4)
          driver.find_element_by_id("image_icon").click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'text_login_title')))
        text_login_title = driver.find_element_by_id("text_login_title").get_attribute("text")
        print "1 Checking the text on the first page: test will fail if \"Log in met je e-mailadres en wachtwoordt\" is not displayed"
        print "Actual result:"
        print text_login_title
        assert text_login_title == 'Log in met je e-mailadres en wachtwoord.'
        #incorrect credentials
        #driver.find_element_by_id("email_address").send_keys("darya.tuzova@salesboard.biz")
        #driver.find_element_by_id("password").send_keys("212123456789")
        #time.sleep(3)
        #driver.press_keycode(4)
        #time.sleep(2)
        #driver.find_element_by_id("login").click()
        # forgot password test NOT IN MVP
        #forgot_pass = driver.find_element_by_id("forgot_password").get_attribute("text")
        #print "1 Checking the text on the first page: test will fail if \"Log in met je e-mailadres en wachtwoordt?\" is not displayed"
        #print "Actual result:"
        #print forgot_pass
        #assert forgot_pass == "Wachtwoord vergeten?"
        #driver.find_element_by_id("forgot_password").click()
        #time.sleep(2)
        #forgot password with incorrect emial
        #driver.find_element_by_id("input_email").send_keys("blablabla.tuzova@salesboard.biz")
        #time.sleep(1)
        #driver.press_keycode(4)
        #time.sleep(1)
        #driver.find_element_by_id("button_submit").click()
        # forgot password with existing email
        #title = driver.find_element_by_id("text_password_recovery_title").get_attribute("text")
        #print title
        #assert title == 'Wachtwoord vergeten?'
        #text_explanation_subtitle = driver.find_element_by_id("text_explanation_subtitle").get_attribute("text")
        #print text_explanation_subtitle
        #assert text_explanation_subtitle == 'Een e-mail met instructies wordt verstuurd om een nieuw wachtwoord mee aan te maken.'
        #time.sleep(1)
        #driver.find_element_by_id("image_action").click()
        #time.sleep(1)
        #driver.find_element_by_id("input_email").send_keys("darya.tuzova@salesboard.biz")
        #time.sleep(1)
        #driver.press_keycode(4)
        #time.sleep(1)
        #driver.find_element_by_id("button_submit").click()
        #driver.press_keycode(4)
        #login
        #driver.find_element_by_id("email_address").click()
        #driver.find_element_by_id("email_address").clear()
        print Variables.email
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'email_address')))
        driver.find_element_by_id("email_address").send_keys(Variables.email)
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'password')))
        driver.find_element_by_id("password").click()
        driver.find_element_by_id("password").send_keys("12345678")
        driver.hide_keyboard()
        try:
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'image_icon')))
          driver.find_element_by_id("image_icon").click()
        except TimeoutException:
          time.sleep(1)
          driver.press_keycode(4)
          driver.find_element_by_id("image_icon").click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'login')))
        driver.find_element_by_id("login").click()
        driver.save_screenshot(Config.screen)
        try:
          WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, 'button_confirm')))
          driver.find_element_by_id('button_confirm').click()
        except TimeoutException:
          pass
        driver.save_screenshot(Config.screen)




