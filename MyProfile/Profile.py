__author__ = 'Darya Tuzova'
import time
from datetime import datetime
import SBTests
import Config
import sys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import re
from Variable import Variables
from selenium.common.exceptions import NoSuchElementException, TimeoutException

class MyProfile(SBTests.TestCase):

    def test_profile(self):
        driver = self.driver
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]')))
        driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]').click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'button_edit_profile')))
        driver.find_element_by_id('button_edit_profile').click()
        driver.save_screenshot(Config.screen)
        print "Original Name and Surname:"
        FirstName = driver.find_element_by_xpath(
            '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[2]/android.widget.LinearLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.EditText[1]').get_attribute(
            "text")
        LastName = driver.find_element_by_xpath(
            '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[2]/android.widget.LinearLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[2]/android.widget.EditText[1]').get_attribute(
            "text")
        EmailText = driver.find_element_by_xpath(
            '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[2]/android.widget.LinearLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[3]/android.widget.EditText[1]').get_attribute(
            "text")
        regex = re.compile("(?<=Voer in. )(.*\n?)(?=. Editing.)")
        email = re.findall(regex, EmailText)
        print FirstName
        print LastName
        print email
        Variables.email = email
        # get first name and lest name and edit them and check again if ther result is expected
        driver.find_element_by_xpath(
            '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[2]/android.widget.LinearLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.EditText[1]').send_keys(
            "Dariia")
        driver.find_element_by_xpath(
            '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[2]/android.widget.LinearLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[2]/android.widget.EditText[1]').send_keys(
            "Sharewire")
        # print FirstName/LastName after change
        print "Changed Name and Surname:"
        FirstName = driver.find_element_by_xpath(
            '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[2]/android.widget.LinearLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.EditText[1]').get_attribute(
            "text")
        LastName = driver.find_element_by_xpath(
            '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[2]/android.widget.LinearLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[2]/android.widget.EditText[1]').get_attribute(
            "text")
        print FirstName
        print LastName
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'button_save')))
        driver.find_element_by_id('button_save').click()
        try:
            WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, 'intercom_toolbar_close')))
            driver.find_element_by_id('intercom_toolbar_close').click()
        except TimeoutException:
            pass
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'text_user_name')))
        Name = driver.find_element_by_id("text_user_name").get_attribute("text")
        print Name
        if Name <> "Dariia Sharewire":
            print "Name wasn't updated"
            assert False
        driver.find_element_by_id('my_influencers').click()
        try:
            WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, 'intercom_toolbar_close')))
            driver.find_element_by_id('intercom_toolbar_close').click()
        except TimeoutException:
            pass
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.ID, 'button_follow')))
        followed = driver.find_element_by_id('button_follow').get_attribute('checked')
        print followed
        if followed <> 'true':
            print "Follow icon should be selected"
            assert False
        driver.save_screenshot(Config.screen)
        try:
            WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, 'intercom_toolbar_close')))
            driver.find_element_by_id('intercom_toolbar_close').click()
        except TimeoutException:
            pass
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'logout')))
        driver.find_element_by_id("logout").click()



