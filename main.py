__author__ = 'Darya Tuzova'
import unittest
import os
import HTMLTestRunner
from Login.LoginApp import *
from Registration.Register import *
import TestResults
import sys
import getopt
import Config
from MyProfile.Profile import *
from MyDay.MyDag import *
from Help.OverDag6 import *
from Search.Search import *
from Browse.News import *
from ReadingList.Leeslijst import *
from Influencers.People import *

import Variable

def execute():
    dir = os.path.dirname(__file__)
    #run test scenario
    suite1 = unittest.TestSuite()
    suite1.addTest((LoginTest('test_onboarding_page')))
    #suite1.addTest((Registration('test_skip_registration')))
    suite1.addTest((Registration('test_regeister_email')))
    suite1.addTest((MyProfile('test_profile')))
    suite1.addTest((LoginTest('test_login_email')))
    suite1.addTest((MyDag('test_myfeed_scroll')))
    suite1.addTest((MyDag('test_myfeed_bookmark_featured_story')))
    suite1.addTest((MyDag('test_myfeed_story_details')))
    suite1.addTest((Zoeken('test_search_special_characters')))
    suite1.addTest((Zoeken('test_search_tags')))
    suite1.addTest((MyDag('test_story_overview')))
    suite1.addTest((Zoeken('test_search_article')))
    suite1.addTest((MyDag('test_story_overview')))
    suite1.addTest((Bladeren('test_trending')))
    suite1.addTest((MyDag('test_story_overview')))
    suite1.addTest((Bladeren('test_actuel')))
    suite1.addTest((MyDag('test_story_overview')))
    suite1.addTest((ReadingList('test_reading_list')))
    suite1.addTest((OverDag('test_help')))
    suite1.addTest((Vakmensen('test_vakmensen_follow')))
    suite1.addTest((MyDag('test_story_overview')))
    suite1.addTest((Vakmensen('test_vakmensen_unfollow')))
    suite1.addTest((Zoeken('test_search_people')))
    suite1.addTest((LoginTest('finish')))#To terminate connection, Driver will have to be re-created
#to do:
#change the slleep to waitdriver
#add more screenshots for all methods



    #run and make report
    dateTimeStamp = time.strftime('%Y-%m-%d %H_%M')
    outdir = os.path.join(dir,'Reports')
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    fileurl = os.path.join(outdir,"TestReport_"+dateTimeStamp+".html")
    buf = file(fileurl,'wb')
    runner = HTMLTestRunner.HTMLTestRunner(stream=buf, title='Test_Result', description='Result of tests')
    runner.run(suite1)



    if TestResults.total_errors != 0:
        print >>sys.stderr, "Found %s errors in total, tests did not pass" % TestResults.total_errors
        sys.exit(2)
    else:
        print >>sys.stderr, "All good, no errors found."


class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def main(argv=None):
    if argv is None:
        argv = sys.argv
    try:
        try:
            opts, args = getopt.getopt(argv[1:], "ho:port:d:p:v:i", ["port","help","output=","deviceName=","platformName=","platformVersion=","udid="])
        except getopt.error, msg:
            raise Usage(msg)


        if (len(opts)==0):
            raise Usage('Please provide client name')

        output = None
        verbose = False
        for o, a in opts:
            if o == "-v":
                verbose = True
            elif o in ("-h", "--help"):
                Usage()
                sys.exit()
            elif o in ("-o", "--output"):
                output = a
            elif o in ("-port", "--port"):
                Config.isRemote = 0
            elif o in ("-d", "--deviceName"):
                Config.isRemote = 0
            elif o in ("-p", "--platformName"):
                Config.isRemote = 0
            elif o in ("-v", "--platformVersion"):
                Config.isRemote = 0
            elif o in ("-i", "--udid"):
                Config.isRemote = 0
            else:
                assert False, "unhandled option"

        execute()

    except Usage, err:
        print >> sys.stderr, err.msg
        print >> sys.stderr, ""
        print >> sys.stderr, "-d |  Sets the device name"
        print >> sys.stderr, "-p |  Sets the platform version"
        print >> sys.stderr, "-v |  Sets platform Version"
        print >> sys.stderr, "-i |  Set device ID"
        return 2

if __name__ == "__main__":
    sys.exit(main())




