__author__ = 'Darya Tuzova'
import time
import SBTests
import Config
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import re


class MyDag(SBTests.TestCase):

      def test_myfeed_scroll(self):
          driver = self.driver
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]')))
          driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]').click()
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]')))
          driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]').click()
          time.sleep(2)
          driver.save_screenshot(Config.screen)
          i = 0
          for i in range(5):
              driver.swipe(200, 300, 200, 100)
              time.sleep(1)
              i += 1
          driver.save_screenshot(Config.screen)
          k = 0
          for k in range(5):
              driver.swipe(200, 200, 200, 400)
              time.sleep(1)
              k += 1
          driver.save_screenshot(Config.screen)


      def test_myfeed_bookmark_featured_story(self):
          driver = self.driver
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'button_bookmark')))
          driver.find_element_by_id('button_bookmark').click()
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'text_story_title')))
          #open details and check if it is bookmarked
          driver.find_element_by_id('text_story_title').click()
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'button_bookmark')))
          Bookmark = driver.find_element_by_id('button_bookmark').get_attribute("selected")
          print Bookmark
          if Bookmark == 'false':
              print "Story was bookmarked in overview,but it is not bokmarked in details"
              assert False
          BottomBarBookmark = driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.widget.LinearLayout[2]/android.widget.ImageView[2]').get_attribute("selected")
          print BottomBarBookmark
          if BottomBarBookmark == 'false':
              print "Story was bookmarked in overview,but it is not bokmarked in bottm bar"
              assert False

      def test_myfeed_story_details(self):
          driver = self.driver
          driver.save_screenshot(Config.screen)
          try:
              WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'intercom_toolbar_close')))
              driver.find_element_by_id('intercom_toolbar_close').click()
          except TimeoutException:
              pass
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'text_likes_count')))
          likecouner_before = driver.find_element_by_id('text_likes_count').get_attribute("text")
          print "Likes before:"
          print likecouner_before
          driver.find_element_by_id('image_like_icon').click()
          driver.save_screenshot(Config.screen)
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'text_likes_count')))
          likecouner_after = driver.find_element_by_id('text_likes_count').get_attribute("text")
          print "Likes after:"
          print likecouner_after
          if int(likecouner_after)-int(likecouner_before)<>1:
              print "Like counter was not encreased"
              assert False
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'image_like_icon')))
          Like = driver.find_element_by_id('image_like_icon').get_attribute("selected")
          print Like
          if Like == 'false':
              print "Story was bookmarked in overview,but it is not bokmarked in details"
              assert False
          LikeBottomBar = driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.widget.LinearLayout[2]/android.widget.ImageView[1]').get_attribute("selected")
          print LikeBottomBar
          if LikeBottomBar == 'false':
              print "Story was bookmarked in overview,but it is not bokmarked in details"
              assert False
          #comments
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'text_comments_count')))
          Reaction = driver.find_element_by_id('text_comments_count').get_attribute("text")
          print "Comment counter after comment was left: " + Reaction
          ReactionBottomBar = driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]').get_attribute("text")
          print "Bottom bar reactions: "+ReactionBottomBar
          ReactionCounter = re.findall('\d+', Reaction)
          for k in ReactionCounter:
              print k
          print "Original number of reaction: "+Reaction
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'comment_icon')))
          driver.find_element_by_id('comment_icon').click()
          driver.save_screenshot(Config.screen)
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'write_comment_container')))
          driver.find_element_by_id('write_comment_container').click()
          driver.save_screenshot(Config.screen)
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'comment_body')))
          driver.find_element_by_id('comment_body').send_keys("Interesting article.")
          driver.find_element_by_id('menu_item_apply').click()
          driver.save_screenshot(Config.screen)
          # liking comment
          FirstCommentLikeCounterBEFORE = driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.support.v7.widget.RecyclerView[1]/android.widget.FrameLayout[3]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.TextView[1]').get_attribute("text")
          print FirstCommentLikeCounterBEFORE
          ComLike = driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.support.v7.widget.RecyclerView[1]/android.widget.FrameLayout[3]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.ImageView[1]')
          ComLike.click()
          FirstCommentLikeCounterAfter = driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.support.v7.widget.RecyclerView[1]/android.widget.FrameLayout[3]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.TextView[1]').get_attribute("text")
          print FirstCommentLikeCounterAfter
          if int(FirstCommentLikeCounterAfter) - int(FirstCommentLikeCounterBEFORE) <> 1:
              print "Like Counter was not increased"
              assert False
              driver.save_screenshot(Config.screen)
          if ComLike.get_attribute("selected") == "false":
              print "Like icon is not selected"
              assert False
          time.sleep(2)
          driver.swipe(200, 200, 200, 300)
          time.sleep(2)
          CommentCounterButtonBar = driver.find_element_by_id('text_comments_count').get_attribute("text")
          print "Comment counter after comment was left: "+ CommentCounterButtonBar
          CommentConter = re.findall('\d+', CommentCounterButtonBar)
          print CommentConter
          for i in CommentConter:
              print i
          time.sleep(2)
          driver.press_keycode(4)



      def test_story_overview(self):
          driver = self.driver
          #change outside
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'button_bookmark')))
          driver.find_element_by_id('button_bookmark').click()
          #WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'image_like_icon')))
          driver.find_element_by_id('image_like_icon').click()
          LikeCounter1 = driver.find_element_by_id('text_likes_count').get_attribute("text")
          print LikeCounter1
          ReactionsCounter1 = driver.find_element_by_id('text_comments_count').get_attribute("text")
          print ReactionsCounter1
          ReactionNumber1 = re.findall('\d+', ReactionsCounter1)
          for a in ReactionNumber1:
              print "First Reaction Number: "+a
          #check inside
          driver.find_element_by_id('image_story_background').click()
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'button_bookmark')))
          Bookmark2 = driver.find_element_by_id('button_bookmark').get_attribute("selected")
          if Bookmark2=="false":
              print "Article is not bookmarked, but it should be"
              assert False
          Like2 = driver.find_element_by_id('image_like_icon').get_attribute("selected")
          if Like2=="false":
              print "Article is not liked, but it should be"
              assert False
          LikeCounterInside2 = driver.find_element_by_id('text_likes_count').get_attribute("text")
          if int(LikeCounterInside2)<>int(LikeCounter1):
              print "Like counters are different"
              assert False
          ReactionCounter2 = driver.find_element_by_id('text_comments_count').get_attribute("text")
          print "Comment counter before comment was left: " + ReactionCounter2
          ReactionNumber2 = re.findall('\d+', ReactionCounter2)
          print ReactionNumber2
          for a in ReactionNumber1:
              for i in ReactionNumber2:
                  print i
                  if int(i)<>int(a):
                      print "Reaction counter wasn't undated inside story"
                      assert False
          #change inside
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'button_bookmark')))
          driver.find_element_by_id('button_bookmark').click()
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'image_like_icon')))
          driver.find_element_by_id('image_like_icon').click()
          driver.find_element_by_id('comment_container').click()
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'write_comment_container')))
          driver.save_screenshot(Config.screen)
          driver.find_element_by_id('write_comment_container').click()
          driver.save_screenshot(Config.screen)
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'comment_body')))
          driver.find_element_by_id('comment_body').send_keys("Interesting article.")
          driver.find_element_by_id('menu_item_apply').click()
          time.sleep(2)
          driver.swipe(200, 200, 200, 300)
          time.sleep(2)
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'text_comments_count')))
          Reaction2 = driver.find_element_by_id('text_comments_count').get_attribute("text")
          print "Comment counter after comment was left: " + Reaction2
          ReactionNumber2 = re.findall('\d+', Reaction2)
          print ReactionNumber2
          for k in ReactionNumber2:
              for a in ReactionNumber1:
                  print k
                  if int(k)-int(a)<>1:
                      print "Reaction counter wasn't increased after comment was left"
                      assert False
          #Go back and check
          time.sleep(2)
          driver.press_keycode(4)
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'button_bookmark')))
          Bookmark3 = driver.find_element_by_id('button_bookmark').get_attribute("selected")
          if Bookmark3 == "true":
              print "Article is not bookmarked, but it should be"
              assert False
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'image_like_icon')))
          Like3 = driver.find_element_by_id('image_like_icon').get_attribute("selected")
          if Like3 == "true":
              print "Article is not liked, but it should be"
              assert False
          LikeCounter3 = driver.find_element_by_id('text_likes_count').get_attribute("text")
          print LikeCounter3
          ReactionsCounter3 = driver.find_element_by_id('text_comments_count').get_attribute("text")
          print ReactionsCounter3
          ReactionNumber3 = re.findall('\d+', ReactionsCounter3)
          print ReactionNumber3
          for k in ReactionNumber2:
              for l in ReactionNumber3:
                  print l
                  if int(l)<>int(k):
                      print "Reaction counter wasn't undated in  story overview after comment was left"
                      assert False









#buttom bar id: bottom_bar

#like of comment //android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.support.v7.widget.RecyclerView[1]/android.widget.FrameLayout[3]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.ImageView[1]
#number of coments like //android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.support.v7.widget.RecyclerView[1]/android.widget.FrameLayout[3]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.TextView[1]
